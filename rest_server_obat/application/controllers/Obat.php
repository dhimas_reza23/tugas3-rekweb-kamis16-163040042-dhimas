<?php

require APPPATH . '/libraries/REST_Controller.php';

class Obat extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Obat_model");
    }

    public function index_get()
    {
        $id = $this->get('id');
        $cari = $this->get('cari');
        if ($cari != "") {
            $obat = $this->Obat_model->cari($cari)->result();
        } else if($id == ""){
             $obat = $this->Obat_model->getData(null)->result();
        }else{
            $obat = $this->Obat_model->getData($id)->result();
        }
        $this->response($obat);
    }

    public function index_put()
    {
        $nama_obat = $this->put('nama_obat');
        $id_jenis = $this->put('id_jenis');
        $stok_obat = $this->put('stok_obat');
        $tgl_exp = $this->put('tgl_exp');
        $harga = $this->put('harga');
        $gambar = $this->put('gambar');
        $keterangan = $this->put('keterangan');
        $data = array(
                'nama_obat'=>$nama_obat,
                'id_jenis'=>$id_jenis,
                'stok_obat'=>$stok_obat,
                'tgl_exp'=>$tgl_exp,
                'harga'=>$harga,
                'gambar'=>$gambar,
                'keterangan'=>$keterangan);
        $update = $this->Obat_model->update('obat', $data, 'id', $this->put('id'));
        if ($update) {
            $this->response(array('status' => 'success', 200));
        }else{
             $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_post()
    {
        // ketikan source code yang ada di modul
        $nama_obat = $this->post('nama_obat');
        $id_jenis = $this->post('id_jenis');
        $stok_obat = $this->post('stok_obat');
        $tgl_exp = $this->post('tgl_exp');
        $harga = $this->post('harga');
        $gambar = $this->post('gambar');
        $keterangan = $this->post('keterangan');
        $data = array(
                'nama_obat'=>$nama_obat,
                'id_jenis'=>$id_jenis,
                'stok_obat'=>$stok_obat,
                'tgl_exp'=>$tgl_exp,
                'harga'=>$harga,
                'gambar'=>$gambar,
                'keterangan'=>$keterangan);
        $insert = $this->Obat_model->insert($data);
        if ($insert) {
            $this->response(array('status' => 'success', 200));
        }else{
             $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_delete()
    {
       // ketikan source code yang ada di modul
        $id = $this->delete('id');
        $delete = $this->Obat_model->delete('obat', 'id', $id);
        if ($delete) {
            $this->response(array('status' => 'success', 201));
        }else{
             $this->response(array('status' => 'fail', 502));
        }
    }
}