<?php

class Obat_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

    public function getData($id = null){
        $this->db->select("*");
        $this->db->from("obat_view");
        if ($id == null) {
            $this->db->order_by('id','asc');
        } else{
            $this->db->where('id', $id);
        }

        return $this->db->get();
    }

    public function cari($cari)
    {
        $this->db->select('*');
        $this->db->from('obat_view');
        $this->db->like('nama_obat', $cari);
        $this->db->or_like('stok_obat', $cari);
        $this->db->or_like('tgl_exp', $cari);
        $this->db->or_like('harga', $cari);
        $this->db->or_like('nama_jenis', $cari);
        $this->db->or_like('keterangan', $cari);
        return $this->db->get();
    }

    public function insert($data){
       // ketikan source code yang ada di modul
        $this->db->insert('obat', $data);
        return $this->db->affected_rows();
    }

    public function update($table, $data, $par, $var) {
        // ketikan source code yang ada di modul
        $this->db->update($table, $data, array($par => $var));
        return $this->db->affected_rows();
    }
    
    public function delete($table, $par, $var){
       // ketikan source code yang ada di modul
        $this->db->where($par, $var);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }
}